Deface::Override.new(
  virtual_path: 'spree/users/show',
  name: 'add_api_details_to_user_account_view',
  insert_after: "[data-hook='account_summary']",
  partial: '../views/spree/users/chargebee.erb'
)
