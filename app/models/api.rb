class Api

  def initialize
    ChargeBee.configure(site: Rails.application.secrets.chargebee_site, api_key: Rails.application.secrets.chargebee_api_key)
  end

  attr_accessor :subs, :user

  def user_subscriptions(user)
    if(self.subs.nil? || self.user.chargebee_id != user.chargebee_id)
      return nil if user.chargebee_id.nil?
      self.user = user
      self.subs = ChargeBee::Subscription.subscriptions_for_customer(user.chargebee_id)
    end
    self.subs
  end

  def self.init
    self
  end

  def mock_resp
    '[{"response":{"subscription":{"id":"HvQquDgOlLKwaj9iF","plan_id":"small_cru_monthly_subscription","plan_quantity":1,"status":"future","start_date":1406665551,"created_at":1406579191,"due_invoices_count":0,"object":"subscription"},"customer":{"id":"2se7Euz5OkZaiXAnwF","first_name":"Grant","last_name":"Ozolins","email":"grant.ozolins@gmail.com","phone":"07568326047","company":"Oaktower Consulting","auto_collection":"off","created_at":1405873687,"object":"customer","card_status":"no_card"}}},{"response":{"subscription":{"id":"2se7Euz5OkZaiXAnwF","plan_id":"large_cru_monthly_subscription","plan_quantity":1,"status":"active","current_term_start":1405873687,"current_term_end":1408552087,"created_at":1405873687,"started_at":1405873687,"activated_at":1405873687,"due_invoices_count":1,"due_since":1405873687,"total_dues":2200,"object":"subscription"},"customer":{"id":"2se7Euz5OkZaiXAnwF","first_name":"Grant","last_name":"Ozolins","email":"grant.ozolins@gmail.com","phone":"07568326047","company":"Oaktower Consulting","auto_collection":"off","created_at":1405873687,"object":"customer","card_status":"no_card"}}}]'
  end
end