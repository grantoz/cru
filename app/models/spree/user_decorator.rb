Spree::User.class_eval do

  def chargebee
    api = Api.new
    api.user_subscriptions(self)
  end

  def subscriptions
    Api.init
    ChargeBee::Subscription.subscriptions_for_customer(self.chargebee_id)
  end

  def chargebee_list
    Api.init
    list = ChargeBee::Customer.list(:limit => 1)
    list.each do |entry|
      customer = entry.customer
      card = entry.card
    end
  end

end