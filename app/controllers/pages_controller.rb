class PagesController < ApplicationController
  def about
  end

  def beans
  end

  def card_updated
  end

  def confirmation
  end

  def contact
  end

  def cru_espresso
  end

  def cru_flat_white
  end

  def faq
  end

  def gifts
  end

  def index
  end

  def pods
  end

  def privacy_policy
  end

  def returns
  end

  def subscribe
  end

  def terms_conditions
  end

  def wholesale
  end
end
