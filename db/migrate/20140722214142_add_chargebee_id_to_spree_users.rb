class AddChargebeeIdToSpreeUsers < ActiveRecord::Migration
  def change
    add_column :spree_users, :chargebee_id, :string, :limit => 50
    add_index :spree_users, :chargebee_id
  end
end
