Rails.application.routes.draw do

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  mount Spree::Core::Engine, :at => '/shop'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#index'

  get 'about',            to: 'pages#about'
  get 'beans',            to: 'pages#beans'
  get 'card-updated',     to: 'pages#card_updated'
  get 'confirmation',     to: 'pages#confirmation'
  get 'contact',          to: 'pages#contact'
  get 'cru-espresso',     to: 'pages#cru_espresso'
  get 'cru-flat-white',   to: 'pages#cru_flat_white'
  get 'faq',              to: 'pages#faq'
  get 'gifts',            to: 'pages#gifts'
  get 'index',            to: 'pages#index'
  get 'pods',             to: 'pages#pods'
  get 'privacy_policy',   to: 'pages#privacy_policy'
  get 'returns',          to: 'pages#returns'
  get 'subscribe',        to: 'pages#subscribe'
  get 'terms_conditions', to: 'pages#terms_conditions'
  get 'wholesale',        to: 'pages#wholesale'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
