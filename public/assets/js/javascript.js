$(document).ready(function(){
	$(".dropdown-toggle").click(function(e) {
		e.preventDefault();
		
		$( this ).next(".dropdown-menu").toggle();
	});

    $(function() {
    $( "#js-video-tabs" ).tabs();
  });



  /// Fixed Nav Bar
   
  $(window).scroll(function() {
    if ($(document).scrollTop() > 300) {
      $(".cta-banner").fadeIn();
    }
    else {
      $(".cta-banner").fadeOut();
    }
  });
  
});
